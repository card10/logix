---
title: Accessories
hidden: true
---

## Gehäuse

- [3d druckbares Gehäuse](https://git.card10.badge.events.ccc.de/card10/cover)

Schau dir die verschiedenen Optionen fuer Gehäuse [hier](/userguide/cases) an.
