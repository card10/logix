---
title: D1 - traveler moone
---

## logbook entry

D1, traveler moone  
Today I discovered that my card10 can pick up information about people's skills in my surroundings and led me to a person with a crazy hat that really knew a thing or two about goldsmithing.  
I actually found a way to transfer my own interests to the card10 and from that point on it started to oscillate softly when picking up similar interests in my vicinity. That's how I found a camp of hedonistic creatures that knew more about security than I could have imagined.  
I don't know if it picks up brainwaves or if it has to do with the blue glow or what else is going on, but at least I learned again that my first impressions can be deceptive after all.

## technical description

There should be a mechanic that allows camps to announce topics they are concerned with via bluetooth beacons and the badges should be able to pick up topics nearby.  
Additionally there could be a privacy friendly way to announce personal topics via the badges and to find people who are concerned with similar topics.

#### demands

* There is a possibility to set up bluetooth beacons that announce a list of topics that the corresponding community is concerned with.
* The badge can receive those announcements
* I can trigger a search for topics in my vicinity via the badge interface (without phone)
* I can see a list of announced topics and which places announced these topics
* I can filter or at least meaningfully sort those topics
* There could be a way to define topics I am interested in (probably via an application on a connected device) and an alarm that triggers when such a topic is somewhere around me
* My personal topics could be announced as well. I should be aware and in control of how and when my topics are announced
* There could be a way to find other people with same topics.
* If I try to contact a person with a similar topic, this person should be able to anonymously decline the the contact before we are "matched"
* There needs to be a way to find that person if we get matched. Maybe the LEDs on our badges show the same randomly generated pattern and we raise our badge wearing arm to find each other.
* other peoples interests shouldn't be discoverable if they set their [personal state leds](/logbook/personal-state-led) to a non-communicative state
