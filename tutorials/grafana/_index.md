---
title: IAQ with Grafana
---

This tutorial will guide you to create a Grafana installation on a Raspberry Pi which
logs temperature, humidity, air pressure and indoor air quality (IAQ) from a card10 badge.
It will look similar to this:

![](grafana.png)

{{% notice warning %}} This guide does not try to secure the installation of all the software packages involved. There might be default passwords still active which are not accounted for here. {{% /notice %}}

The individual steps are:

- Prepare card10
   - Turn on Bluetooth
   - Turn on BSEC in BME680 app
- Prepare Raspberry Pi 3/4
   - Install latest "Raspberry Pi OS Lite" onto an SD card
   - Boot Raspberry Pi
   - Login
   - Change the password via `passwd`
   - Configure any network interfaces as needed using `rpi-config`
   - `sudo apt update`
   - `sudo apt upgrade`
   - `sudo reboot`
   - `sudo apt install git`
 - Install IOTstack
   - `git clone https://github.com/SensorsIot/IOTstack.git IOTstack`
   - `cd ~/IOTstack`
   - `./menu.sh`
   - Let it install dependencies
   - Restart when asked
   - Edit `/etc/group` and add `pi` to the `docker` group: `docker:x:995:root,pi`
   - Logout
   - Login again, run `menu.sh` again
   - Ignore error message because of docker version
   - Select "Build Stack"
   - Select grafana, influxdb, nodered using space
   - Enter the nodered menu to build addons list with the right arrow key

   ![](iotstack-1-turn-on-nodered.png)

   - Press enter

   ![](iotstack-3-build-addons.png)

   - Add `node-red-contrib-generic-ble` using space

   ![](iotstack-2-select-generic-ble.png)

   - Hit enter to build `addons_list.yml`
   - Hit escape to go back
   - Hit enter to start the build
   - Go to "Docker Commands"
   - Select "Start stack"
   - Wait for containers being built
   - Press enter
   - Press escape

 - Pair card10 to system
   - Run `bluetoothctl`
   - `scan on`
   - Enter Bluetooth menu on card10
   - Wait until something like `[NEW] Device CA:4D:10:XX:XX:XX card10-xxxxxx` appears
   - `pair CA:4D:10:XX:XX:XX` (replace with MAC that is shown in bluetooth menu)
   - Type `yes`, hit yes on card10
   - Try again if the pairing fails
   - `disconnect`
   - `exit`

 - Create influx database
   - `docker exec -it influxdb influx`
   - `create database card10`
   - (use card10)
   - `create user "nodered" with password 'supersecure'`
   - `create user "grafana" with password 'supersecure'`
   - `grant all on card10 to nodered`
   - `grant all on card10 to grafana`
   - `exit`

- Configure grafana
   - Go to `<IP of your Raspberry Pi>:3000`
   - User `admin`, Password `admin`
   - Select a new password
   - Add new data source influxdb

   ![](grafana-1.png)
   ![](grafana-2.png)
   ![](grafana-3.png)

   - Url: `http://influxdb:8086`

   ![](grafana-4.png)

   - Database: `card10`
   - User: `grafana`
   - Password: `supersecure`
   - Method: GET

   ![](grafana-5.png)

   - Press "Save & test"

   ![](grafana-6.png)
   ![](grafana-7.png)

   - Import [Air-1641131410916.json](Air-1641131410916.json)

   ![](grafana-8.png)

   - Select the InfluxDB data source you created before

   ![](grafana-9.png)

   - Press Import

 - Add card10 in nodered
   - Go to `<IP of your Raspberry Pi>:1880`
   - Import [card10-iaq-flow.json](card10-iaq-flow.json)

   ![](nodered-1.png)
   ![](nodered-2.png)

   - Change mac of device to `CA:4D:10:XX:XX:XX` (same as above)

   ![](nodered-3.png)
   ![](nodered-4.png)

   - Set user of influxdb to `nodered`
   - Set password of influxdb to `supersecure`

   ![](nodered-9.png)
   ![](nodered-6.png)
   ![](nodered-7.png)

   - Deploy flow
   - Reboot (takes some time)
   - Check that card10 BLE connection goes to "Active"

You should now see data coming in in the grafana dashboard that you created before under `http://<IP of your Raspberry Pi>:3000`

