---
title: Getting Started
---

{{% notice info %}}
Deutsche Version: [Los geht’s!](../gettingstarted.de)
{{% /notice %}}

You just received your card10 - what now?

## card10 assembly

[Here](/userguide/assembly)'s a step by step guide with a video and pictures for assembling your card10

## User guide

Have a look at the [General Usage](/userguide/general-usage) section of the [User Guide](/userguide). It will tell you how to:
 - Switch the card10 on and off
 - Navigate the menu
 - Install new apps
 - Set your nickname

And much more!

### Setting your nickname
To set your nickname, create a file called `nickname.txt` on your laptop. 
Start your card10 in USB storage mode, then copy `nickname.txt` onto the card10. Eject the USB device.
Then briefly press the POWER button to exit the storage mode, and then a second time, to enter the menu.
You can scroll up and down using the LEFT and RIGHT buttons. 
Now look for an app called 'nickname.py', select it by pressing the SELECT button.
If you have successfully uploaded a `nickname.txt` file, your nickname will now show up on the display.
You can also upload a `nickname.json` file instead, to get some more options. 
Have a look at the [hatchery](https://badge.team/projects/card10_nickname) for more options of the app.

### Basic ECG howto
Have a look at [how to use the ECG](../ecg/).

For better results use [USB-C electrodes](../ecg_kit_assembly/)


## Next
Now it is time to start playing with the card10, writing your own micropython code!
Continue in the [first interhacktions](/interhacktions/firstinterhacktions) section.
