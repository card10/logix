---
title: User Guide
weight: 10
---

{{% notice warning %}}
THIS USER GUIDE IS IN ITS EARLY STATE OF CONSTRUCTION. IT IS INCOMPLETE.
{{% /notice %}}

The user guide of the card10 is divided into a few sections. Here is an overview of the different sections and what they cover:

### [Assembly](assembly)
 - [Assembly video](assembly#assembly-video)
 - [Assembly instructions](assembly#assembly-instructions)

### [General usage](general-usage)
 - [Buttons](general-usage#buttons)
 - [Powering on](general-usage#powering-on)
 - [Expected run time](general-usage#expected-run-time)
 - [Going to standby](general-usage#going-to-standby)
 - [Powering off](general-usage#powering-off)
 - [Starting apps](general-usage#starting-apps)
 - [Setting your personal state](general-usage#setting-your-personal-state)
 - [Installing apps via USB](general-usage#installing-apps-via-usb)


### [Bluetooth Low Energy (BLE)](ble)
 - [Enabling and disabling BLE](ble#enabling-and-disabling-ble)
 - [Pairing](ble#pairing)
   - [Creating a pairing](ble#creating-a-pairing)
   - [Removing a pairing](ble#removing-a-pairing)
 - [Checking if a device is already connected](ble#checking-if-a-device-is-already-connected)
 - [Installing the companion app on Android](ble#installing-the-companion-app-on-android)
 - [Installing the companion app on iOS](ble#installing-the-companion-app-on-ios)
 - [Using the light sensor with Phyphox](ble#using-the-light-sensor-with-phyphox)
 - [Using characteristics via BLE](ble#using-characteristics-via-ble)

### [The menu system](menu-system)
- [Selecting a default app](menu-system#selecting-a-default-app)
- [Changing the button layout](menu-system#changing-the-button-layout)

### [Using the card10 as a watch](watch)
- Setting the time without BLE
- Setting the time with BLE
- Apps which implement watch features

### [Using the card10's ECG functionality](ecg)
- Configuring the ECG app correctly
- Using it correctly
- Taking a recording
- Installing wired connections


### [Cases](cases)
- 3D printed cases
- Other variants
