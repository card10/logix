---
title: "Bluetooth Low Energy (BLE)"
weight: 30
---

BLE is a low power (and low throughput) radio communication standard. Currently the card10 only implements the peripheral role and can be connected to one other device at a time. The other device acts as the central and is usually a smartphone or a regular computer.

**Note:** All information on this page relates to firmware versions including and following release 1.16 (Pandemic Potato). Before this release BLE support was handled differently. All pairings created with release 1.15 and before become invalid with release 1.16.

## Configuring BLE
### Enabling and disabling BLE
By default BLE is turned off. This has multiple reasons:
 - BLE slightly raises the power consumption
 - If paired to another device, BLE can impact your privacy. Currently a paired card10 can be tracked (it does not implement BLE privacy features)
 - An active BLE interface can pose a security risk (as to all wireless interfaces).

If you want to make use of the BLE features, you therefore have to enable BLE first.

BLE can be enabled and disabled in the `Bluetooth` app. The app will tell you if BLE is enabled or not:

![Going to standby](/media/ug/2-ble-menu.jpg)
![Going to standby](/media/ug/2-ble-off.jpg)


Use the `SELECT` button to enable or disable BLE. Your card10 will restart to apply the changes after pressing the button. This is normal.

![Going to standby](/media/ug/2-ble-ready.jpg)

After enabling BLE for the first time you can continue to create a BLE pairing:

## Pairing
### Creating a pairing
Generally the card10 only allows interaction with other devices if they have paired to he card10 before.

To create a pairing open the `Bluetooth` app:

![Going to standby](/media/ug/2-ble-menu.jpg)
![Going to standby](/media/ug/2-ble-ready.jpg)

Your card10 is now in pairing mode and can be seen by other devices. You can use the `card10companion` on Android or the system dialog of other devices to find your card10 and start the pairing process:

Android:
{{< figure src="/media/ug/2-ble-android-1.png" link="/media/ug/2-ble-android-1.png" width="300px" >}}
iOS:
card10companion:

Once the pairing process has started both devices will show a confirmation code. Make sure that they both show the same code and accept the pairing on both. If possible the card10 will also show a name of the device which is trying to pair:

![Going to standby](/media/ug/2-ble-confirmation.jpg)

**Hint:** On Android this dialog sometimes pops up in the background and has to be selected from the status bar.

Android:
{{< figure src="/media/ug/2-ble-android-2.png" link="/media/ug/2-ble-android-1.png" width="300px">}}
iOS:
card10companion:

If the pairing process was successful, the card10 will show the following message:

![Going to standby](/media/ug/2-ble-success.jpg)

It will also tell you the name of the pairing which was created. You can use this name to later delete it again.


If the pairing process failed, the card10 will show an error message:

![Going to standby](/media/ug/2-ble-fail.jpg)

This might be due to several issues:
 - Your phone/computer might not yet support BLE 4.2 which implements the security features required by the card10
 - You did not confirm the pairing in time. Make sure to locate the confirmation dialog on you phone. Especially on Android.
 - BLE implementations frequently show bugs. Consider restarting both devices.

### Removing a pairing
If you don't trust a pairing anymore or want to prevent the other device from automatically connecting to your card10, you can remove an individual pairing. To do this go the the `USB Storage` mode, navigate to the `pairings/` folder and delete the pairing you want to remove:

Make sure to always us the `eject safely` feature of your operating system before leaving USB storage mode!.


## Checking if a device is already connected
The card10 can only be connected to one other device at a time. If you can't connect with a particular device it might be the case that another device is already connected. You can check if this is the case in the `Bluetooth` app. If another device is connected, it's name will be shown there:

![Going to standby](/media/ug/2-ble-active.jpg)

## Installing the companion app on Android
The companion app for Android allows you to:
 - Set the current time and date
 - Install or update apps
 - Set the personal state
 - Upload files
 - Make your card10 sparkle

You can find the latest release of the Companion App in the F-Droid repository:

[Companion App for the card10 Chaos Communication Camp badge](https://f-droid.org/en/packages/de.ccc.events.badge.card10/)

Have a look at the [app section](/app) for more information about the app.

## Installing the companion app on iOS
Currently there is no regular iOS release of the card10 companion available. If you want to help out, **please** get in touch:

What you can to on iOS without the app:
 - Get the current time and date (including time zone)
 - *Probably* use Phyphox to read out sensors

## Using the light sensor with Phyphox
Phyphox is a versatile physics experimentation app for smartphones. It can use the internal sensors of a smartphone, but it can also use external sensors via BLE. Currently only the light sensor is exposed via BLE. This might change in future firmware releases.

To use the light sensor in Phyphox:

TBD

## Using characteristics via BLE
How to use nRF Connect goes here.


