---
title: "Watch usage"
weight: 50
---

## Setting the time and date
### Using an iOS device
As iOS offers a time service via bluetooth, you can use a bluetooth connection to your iOS device to sync the time to the watch.
1. Enable bluetooth on your watch
2. Connect to the watch using your iOS device
## Watch face options
## Accuracy
## Energy consumption
## Interact with your watch
### on iOS using phyphox
1. Disconnect your iOS device from the watch
2. Download phyphox to your iOS device: https://itunes.apple.com/us/app/phyphox/id1127319693?l=de&ls=1&mt=8
3. Go to the following url on your PC and scan the QR Codoe via the phyphox app  https://phyphox.org/wiki/index.php/Card10
4. Connect to the watch via the dialog of phyphox
5. Read supported functions (currently only ther light sensor)
