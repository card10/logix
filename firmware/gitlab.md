---
title: GitLab
---


# Hooray 🎉

## Post-Registration

Welcome to card10 and flow3r badge GitLab instance.

**1st**: *Check* your *mails*, links below won't work until verification.

You can find this text at our wiki: https://card10.badge.events.ccc.de/firmware/gitlab/

### What's next

To get an understanding of card10 (cardio) checkout:

- our [Flow3r Docs](https://docs.flow3r.garden)
- explore our [GitLab Server](https://git.flow3r.garden/explore/groups).
   - also checkout our public repos at [`/flow3r`](https://git.flow3r.garden/flow3r/).

### Profile Settings
Keep this tab open, accept your registration mail and you can go to this links.

- Updating Your [Profile Privacy](https://git.flow3r.garden/profile)
- Adding [Two-Factor/OTP](https://git.flow3r.garden/profile/two_factor_auth) 
- Change [E-Mail Notifications](https://git.flow3r.garden/profile/notifications)
- Add your [SSH Key](https://git.flow3r.garden/profile/keys) for `git clone...`
