---
title: Developing
---

You can find the repositories for the apps here:

* [git.card10.badge.events.ccc.de/card10/companion-app-android](https://git.card10.badge.events.ccc.de/card10/companion-app-android/)
* [git.card10.badge.events.ccc.de/card10/companion-app-ios](https://git.card10.badge.events.ccc.de/card10/companion-app-ios/)

Please join the channel [`#freenode_#card10companion:matrix.org`](https://riot.im/app/#/room/#freenode_#card10companion:matrix.org)

### Android / F-Droid nightly

The GitLab delivere a (unsafe) Android Debug Build of Companion App by a F-Droid repository.
If you are able to create debug logs and create Issues feel free to subscribe this repository.


[![https://git.card10.badge.events.ccc.de/card10/companion-app-android-nightly/raw/master/fdroid/repo](https://git.card10.badge.events.ccc.de/card10/companion-app-android-nightly/raw/master/icon.png)](https://git.card10.badge.events.ccc.de/card10/companion-app-android-nightly/raw/master/fdroid/repo)

### iOS Testflight builds

**[https://testflight.apple.com/join/oqZhY70M](https://testflight.apple.com/join/oqZhY70M)**

 ⚠️ Please note ⚠️ We've got a lot of trouble pairing with the card10. If your want to connect to the card10:
 
* checkout the [ios-workarounds branch](https://git.card10.badge.events.ccc.de/card10/firmware/commits/plaetzchen/ios-workaround) of the firmware repository
* rebase the branch on the current firmware master
* build a firmware image
* flash that image on the card10
* The ios-workarounds branch basically exposes all characteristics publicly, so everybody can connect to your card10 and read / write characteristic, so this is clearly only a development helper and not ment for day-to-day usage

**OR**

* Use [this file](https://git.card10.badge.events.ccc.de/card10/logix/blob/master/app/card10-ios-branch.bin) and transfer it as *card10.bin* over USB to your card10 

[![https://testflight.apple.com/join/oqZhY70M](http://api.qrserver.com/v1/create-qr-code/?color=000000&bgcolor=FFFFFF&data=https%3A%2F%2Ftestflight.apple.com%2Fjoin%2FoqZhY70M&qzone=1&margin=0&size=400x400&ecc=L)](https://testflight.apple.com/join/oqZhY70M)
