---
title: Interhacktions
---

## General considerations
### Privacy
Do not collect unneeded data. Always ask yourself if you can reduce the amount of needed data further. Avoid keeping logs.

### Different personal space of people
Make sure your InterHacktions don't encourage people to grab or otherwise uncomfortly interact with people who did not consent.

### Chirality
Some people are left-handed, others are right-handed. Some place a watch below the wrist, some above. When designing user interfaces, bear in mind that the display output may be rotated and buttons may be in a different position than you expect.

### Accessibility
Consider the variability in range of motion, vision, etc. New ideas how menus can be navigated and the HERT2 can become more accessible are very welcome.

## Interfaces
### Physical

#### Contacts for sewing or soldering
A total of 4 GPIO pins, as well as 3.3V and GND contacts are available for sewing wearable LEDs, sensors, ... onto the wristband.

The pitch of the small holes between the sewable connectors fits a 2.54 mm pin header, so if you rather work with solder than thread, you can use an angled pin header to access the sewable connectors. Additional debugging pins are also available through the small contacts between the five sewable connectors on each side.

#### USB-C spare pins
Besides the usual USB pins, we made some special signals e.g. UART and contacts for attaching ECG probes available on the USB-C connector. You can find more details on the [USB-C](/hardware/usbc/) page.


#### Wristband
If the velcro lined wristband doesn't fit your needs, you can use the holes for the sewing contacts or the ECG contacts to attach your own wristband.

#### Harmonic Board
The Harmonic Board is a fairly simple 4 layer board, you can make a customized Harmonic Board if you like.

### Wireless Interfaces
#### Bluetooth Low Energy
Below an overview of BLE interhacktions; for more details see the [ble page](/userguide/ble)
##### Badge to badge
##### Badge to app
##### Badge to interhactions, latptop, etc.
##### Mesh


## Software
### Micropython l0dables
### c l0dables
#### Air Guitar
### Files read as a USB drive
