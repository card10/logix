---
title: tl;dr
hidden: true
---

- [Hardware](http://git.card10.badge.events.ccc.de/card10/hardware) - hardware repo mit docs und specs
- [Firmware](http://git.card10.badge.events.ccc.de/card10/firmware) - firmware repo
- [Firmware Docs](https://firmware.card10.badge.events.ccc.de/) - rendered version der firmware-doku
- [Assembly guide](/userguide/assembly.de/) - bau dein card10 zusammen
- [LogBook](/logbook) - Geschichten der Reisenden
- [Interhacktions](/interhacktions) - eine Anleitung, um Apps zu bauen
- [Hardware-Übersicht](/hardware)
- [USB-C](/hardware/usbc)
- [personal state](/ps)
- [app](/app)
- [zusammenbau-video](/userguide/assembly#assembly-video)
