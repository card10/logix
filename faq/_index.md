---
title: FAQ
---

{{% notice info %}}
Deutsche Version: [FAQ](faq.de)
{{% /notice %}}

## How can I contribute?
Generally, there is a wide range of contributions that can be made to the firmware, the iOS and Android apps, the various methods of BLE communication, the user interface, the documentation, by reviewing existing work, and of course by creating your own [interhacktions](/interhacktions)!

If you don't know where to get started you can also have a look at some of the ideas for contributions we outlined [here](https://git.card10.badge.events.ccc.de/card10/logix/issues). If you have questions on the issue, please don't hesitate to comment or get in touch to the creator of the issue through our [Matrix](https://en.wikipedia.org/wiki/Matrix_(protocol)):( [`#freenode_#card10badge:matrix.org`](https://matrix.to/#/#freenode_#card10badge:matrix.org))/[IRC](https://en.wikipedia.org/wiki/Internet_Relay_Chat) ( [`freenode.net#card10badge`](ircs://chat.freenode.net:6697/card10badge)) channel.

You can also be a great help by adding documentation to the [card10logix wiki](https://card10.badge.events.ccc.de). If you notice some information is missing, but you don't know the information that needs adding, you can also make us aware by [adding an issue](https://git.card10.badge.events.ccc.de/card10/logix/issues). Please make sure to describe in detail what information is missing.

## How does the so-called wiki work?
To keep things easier to maintain (the r0ket wiki has been going for 8 years now!), we decided on a git/markdown based wiki for this year.
Editing the wiki works as follows: Click 'edit' on the top right of a page. Create an account for the gitlab interface that opens. Once you have made your changes, you will be guided through the process of generating a pull request. If you are still working on your edits, please add the flag 'WIP' before the summary of your pull request. The pull request will then be ignored until you remove the flag.
If you have any questions, don't hesitate to get in touch via the card10 communication channels.

## What is all this stuff about travelers and researchers?
This year's badge isn't only hardware and firmware, it comes with some story telling around it. All work before day0 is performed by 'researchers'. According to the storytelling, they received some messages from the future, indicating that cccamp19 will have a badge. These reports come from 'travelers', which within the story are people at camp, from day 1 onwards. In reality these reports are our means of introducing card10 features and concepts, but wrapped into a nice story.

## Will the wristband be included with the card10?
*Whilst at most times #card10 is observed wrist mounted on a very soft, fluffy wrist band, occasionally I spotted a card10 in unusual places, fitted on top of clothing, extended with more LEDs and sometimes also combined with other electronic boards. These boards often have the shape of r0kets, but I also noticed several other creative outlines and colourful blinking patterns.*

Yes, a comfortable neoprene wristband comes with every card10. If you want something else, you can of course bring your own, and attach it either with the screw bars that also attach the neoprene wristband, or by sewing your card10 onto your wristband. The latter is also an option with the neoprene wrist band, if you are concerned about an allergic reaction to the metal clamps, however this reduces the EKG functionality. To maintain skin contact and thus EKG function, you can instead attach a piece of conductive silver fabric on the skin side of your wristband and connect it to the EKG (TODO: add more details how to do this). You can even attach your card10 to anything else you can come up with, using the screwbars or needle and thread!

We recommend reinforcing the velcro hook side attachment by adding a seam along the sides of the velcro hook side piece.

## Why should I consider keeping the protective foil on the display?
The display has a polarizing filter. With some sunglasses this has the unfortionate effect that the card10 is difficult to read on your wrist. If you keep the protective foil stuck on top, the filter is effectively disabled.

## What can I do if the filesystem of my badge is read-only?
tl;dr: `fsck`

The [maintenance and repair](/maintenance) section of card10logix includes information about [filsystem repairs](/maintenance/filesystem).

## Help! My Display is not working / it cracked
During camp we discovered that the badge display is one of the parts that easily gets damaged.
If you get a crack due to a mechanical crash or similar incident you have to replace your display.

The [maintenance and repair](/maintenance) section of card10logix includes a [guide for display replacement](/maintenance/display).


