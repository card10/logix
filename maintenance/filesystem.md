---
title: File system maintainance
---
## Read only file system

One possible reason for a read-only filesystem is a set "dirty-bit".
To change this the linux tool fsck can be used.
First the badge needs to be ejected safely, then it needs to be restartet and put back into "USB storage" mode.
The device should not be mounted. Now `lsblk` can help with finding the correct blockdevice.
Then you can check the filesystem with `fsck /dev/sdX` (replace `sdX` with the correct device).
If neccessary the tool will ask if the "dirty bit" should be removed, this needs to be answered with yes.
After this, the problem should be solved.

## No file system detected
If operating does not show a file system after activating _USB Storage Mode_ and attaching it via USB, a few things might be at fault:

 - The file system might be corrupted. Please try to create a new FAT file system and copy over the data from a working firmware release.
 - The USB cable might be broken. Check if your computer recognized a new device.


