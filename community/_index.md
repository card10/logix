---
title: Community
weight: 10
---

Please get in touch with us. We are always happy to help out and get your card10 in shape!

## Chat
  - [IRC](https://en.wikipedia.org/wiki/Internet_Relay_Chat): [`libera.chat#card10badge`](https://web.libera.chat/#card10badge)
  - mirrored to [Matrix](https://en.wikipedia.org/wiki/Matrix_(protocol)) (mirror): [`#card10badge:libera.chat`](https://matrix.to/#/#card10badge:libra.chat)
    - If you can please join via IRC directly. Bridging IRC channels to Matrix is unreliable at best.

## Social Media
  - [`@card10badge@chaos.social`](https://chaos.social/@card10badge)
  - [`twitter.com/card10badge`](https://twitter.com/card10badge)

## Bug reports and development
  - [GitLab: `https://git.card10.badge.events.ccc.de`](https://git.card10.badge.events.ccc.de/explore/groups/)

You can [browse](https://git.card10.badge.events.ccc.de/card10/logix) this wiki's source and [read more](/firmware/gitlab) about our GitLab instance.
