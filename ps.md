### Personal States

You can use your card10 to communicate your personal state to others, and find out about the state of others. 
There are four states indicating that you are spending time for yourself, feel communicative, want to discover new things, or are occupied with camp(ing) tasks.

Four personal states are defined:

#### no contact, please!
`_______ red led, continuously on`

*I am overloaded. Please leave me be*

#### chaos
`. _ . _ blue led, short blink, long blink`

*Adventure time*

#### communication
`_ _ _ _ yellow led, long blinks`

*I want to learn something or have a nice conversation*

#### camp
`<> <> < green led, fade on and off`

*I am focussed on self-, camp-, or community maintenance*

The mode can be activated via a card10 app, and is indicated with the top left LED lighting up the space between the fundamental and the harmonic board.
You can also implement it on paper, if you run out of battery, by indicating the colour/modename/blinking pattern
## Please respect the 'no contact, please' state of others.



### Persönliche Zustandsanzeige

Du kannst dein card10 um deinen persönlichen Zustand an andere zu kommunizieren und herausfinden, wie es anderen geht.
Die vier Zustände zeigen an, ob du gerade Zeit alleine verbringen möchtest, kommunikativ bist, neue Dinge entdecken möchtest oder mit Camp(ing)-Aufgaben beschäftigt bist.

Es sind vier persönliche Zustände bereits definiert: 

#### bitte kein Kontakt!!
`_______ rote LED, dauerhaft an`

*Ich bin gerade überlastet, bitte lass mich in ruhe*

#### Chaos
`. _ . _ blaue LED, kurzes blinken, langes blinken`

*Abenteuer-Zeit*

#### Kommunikation
`_ _ _ _  gelbe LED, langes blinken`

*Ich möchte etwas lernen oder eine nette Unterhaltung führen*

#### Camp
`<> <> < grüne LED, blendet ein und aus`

*Ich bin gerade mit selbst-, camp- oder gemeinschafts-fürsorge beschäftigt*

Der Modus kann über die card10-App aktiviert werden und wird über die linke obere LED angezeigt, die den Raum zwischen dem "fundamental" und dem "harmonic"-board beleuchtet
Du kannst es auch auf auf papier implementieren, wenn der akku alle ist, indem du die Farbe, den Modus-Namen oder das Blinke-Muster draufmalst.
## Bitte respektiert den "bitte kein Kontakt"-Modus von anderen!
